import { useState, useEffect } from 'react';
import './App.css';
import Navbar from './components/AppNavbar';
import Landing from './pages/Home';
import Footer from './components/Footer';
import Products from './pages/Catalog';
import ProductView from './pages/ProductView';
import Login from './pages/Login';
import Signup from './pages/Register';
import Logout from './pages/Logout';
import Dashboard from './pages/Dashboard';
import Error from './pages/Error';

import { UserProvider } from './UserContext';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';

export default function App() {

  const [ user, setUser ] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    
    localStorage.clear();

    setUser({
      id: null,
      isAdmin: null
    })
  }

  useEffect(() => {
    
    console.log(user);

    fetch('https://calm-sierra-46560.herokuapp.com/users/details', {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
      } 
    }).then(resultOfPromise => resultOfPromise.json()).then(convertedResult => {
      console.log(convertedResult);
      if (convertedResult._id !== "undefined") {
        setUser({
          id: convertedResult._id,
          isAdmin: convertedResult.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])

  return (
    <UserProvider value={{user, unsetUser, setUser}}>
      <Router>
        <Navbar />

        <Switch>
          <Route exact path="/" component={Landing} />
          <Route exact path="/products" component={Products} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Signup} />
          <Route exact path="/logout" component={Logout} />
          <Route exact path="/products/:productId" component={ProductView} />
          <Route exact path="/dashboard" component={Dashboard} />
          <Route component={Error} />
        </Switch>

        <Footer />

      </Router>
    </UserProvider>
  );
}

