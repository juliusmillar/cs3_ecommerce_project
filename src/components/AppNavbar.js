import { useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {

	const { user } = useContext(UserContext);

	return(
		<Navbar className="navbar bg-dark" expand="lg">
			<Navbar.Brand className=" navbarBrand text-light">
				3 Cards Computer Hub
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav.Link as={NavLink} to="/">Home</Nav.Link>
				<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
				{(user.id) ?
					<>
						<Nav.Link as={NavLink} to="/dashboard">Admin Dashboard</Nav.Link>
						<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
					</>
				:
					<>
						<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
						<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
					</>
				}
			</Navbar.Collapse>
		</Navbar>
	)
}