import React from 'react-bootstrap';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function Footer() {
	return(
		<div className="footer bg-dark font-weight-bold border-secondary" >
			<Row className="footer-row text-light">
				<Col>
					E-Commerce App
				</Col>
				<Col>
					3 Cards Computer Hub
				</Col>
				<Col>
					© 2021 Copyright: Julius Millar
				</Col>
			</Row>
		</div>
	);
}