import { Row, Col } from 'react-bootstrap';

export default function Banner({info}) {

	const { title, tagline } = info;

	return(
		<Row>
			<Col className="p-5">
				<h1> {title} </h1>
				<p> {tagline} </p>
			</Col>
		</Row>
	)
}