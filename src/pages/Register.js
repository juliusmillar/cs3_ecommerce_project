import { useState, useEffect } from 'react';
import {Form, Button, Container } from 'react-bootstrap';
import Hero from '../components/Banner';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';

const paraBanner = {
	title: "Account Registration",
	tagline: "Create your own account here!"
}

export default function Register() {
	
		const history = useHistory();
		const [ firstName, setFirstName ] = useState('');
		const [ middleName, setMiddleName ] = useState('');
		const [ lastName, setLastName ] = useState('');
		const [ email, setEmail ] = useState('');
		const [ password1, setPassword1 ] = useState('');
		const [ gender, setGender ] = useState('Male');
		const [ password2, setPassword2 ] = useState('');
		const [ mobileNo, setMobileNo ] = useState('');
		const [ isRegisterBtnActive, setRegisterBtnActive ] = useState('');
		const [ isComplete, setIsComplete ] = useState(false);
		const [ isMatched, setIsMatched ] = useState(false);


		function registerUser(event) {
			event.preventDefault();

			console.log(firstName);
			console.log(middleName);
			console.log(lastName);
			console.log(email);
			console.log(password1);
			console.log(gender);
			console.log(mobileNo);

			fetch('https://calm-sierra-46560.herokuapp.com/users/register', {
				method: "POST",
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					middleName: middleName,
					lastName: lastName,
					email: email,
					password: password1,
					mobileNo: mobileNo,
					gender: gender
				})
			})
			.then(res => res.json())
			.then(data => {
				if (data) {
					Swal.fire({
						title: `Hey ${firstName}, your account is registered successfully`,
						icon: 'success',
						text: 'Welcome to 3 Cards E-Commerce App!'
					})
					history.push('/login');
				} else {
					Swal.fire({
						title: `Registration Failed`,
						icon: 'error',
						text: 'Please try to register again!'
					})
				}
			})
		} 

		useEffect(() => {

			// creating a control strucutre that will validate the values of the input fields.
			if ((firstName !== '' && middleName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && mobileNo !== '') && (password1 === password2)) {
				setRegisterBtnActive(true);
				setIsComplete(true);
			} else {
				setRegisterBtnActive(false);
				setIsComplete(false);
				
			}
		}, [firstName, middleName, lastName, email, password1, password2, mobileNo]);

		useEffect(() => {
			if ((password1 === password2) && (password1 !== '' && password2 !=='')) {
				setIsMatched(true);
			} else {
				setIsMatched(false);
			}
		}, [firstName, middleName, lastName, email, password1, password2, mobileNo]); // it will ask for an "optional" parameter (dependencies list) // useEffect === eventListeners

	return(
		<Container>
			{/*Banner/Greetings*/}
			<Hero info={paraBanner} />

			{/*Find a way to make this component "reactive"*/}
			{
				isComplete ? 			
					<h1 className="mb-5"> Proceed with Register! </h1>
				:			
					<h1 className="mb-5"> Fill up the Form Below </h1>
			}


			{/*Register Fom*/}
			<Form onSubmit={(event) => registerUser(event) } className="mb-5">
				{/*First Name*/}
				<Form.Group controlId="firstName">
					<Form.Label>
						First Name:
					</Form.Label>
					<Form.Control
						type="text"
						placeholder="Insert First Name Here" value={firstName}
						onChange={event => setFirstName(event.target.value)}
						required
					/>
				</Form.Group>

				{/*Middle Name*/}
				<Form.Group controlId="middleName">
					<Form.Label>
						Middle Name:
					</Form.Label>
					<Form.Control
						type="text"
						placeholder="Insert Middle Name Here" value={middleName}
						onChange={pangyayari => setMiddleName(pangyayari.target.value)}
						required
					/>
				</Form.Group>

				{/*Last Name*/}
				<Form.Group controlId="lastName">
					<Form.Label>
						Last Name:
					</Form.Label>
					<Form.Control
						type="text"
						placeholder="Insert Last Name Here" value={lastName}
						onChange={scene => setLastName(scene.target.value)}
						required
					/>
				</Form.Group>

				{/*Email Address*/}
				<Form.Group controlId="userEmail">
					<Form.Label>
						Email Address:
					</Form.Label>
					<Form.Control
						type="email"
						placeholder="Insert Email Here" value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
				</Form.Group>

				{/*Password*/}
				<Form.Group controlId="password1">
					<Form.Label>
						Password:
					</Form.Label>
					<Form.Control
						type="password" 
						placeholder="Insert Password Here" value={password1} 
						onChange={e => setPassword1(e.target.value)}
						required
					/>
				</Form.Group>

				{
					isMatched ?
					// truthy
						<p className="text-success"> *Passwords Matched!* </p>
					:
					// falsy
						<p className="text-danger"> *Passwords should Match!* </p>
				}


				{/*Confirm/Verify Password*/}
				<Form.Group controlId="password2">
					<Form.Label>
						Confirm Password:
					</Form.Label>
					<Form.Control 
						type="password" 
						placeholder="Confirm Password Here" value={password2} 
						onChange={e => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>

				{/*Select option for Gender*/}
				<Form.Group controlId="gender">
					<Form.Label>
						Gender:
					</Form.Label>
					<Form.Control as="select" value={gender} onChange={(e) => setGender(e.target.value)}>
						<option selected disabled>Select Gender Here</option>
						<option value="Male" >Male</option>
						<option value="Female" >Female</option>
					</Form.Control>
				</Form.Group>

				{/*Mobile Number*/}
				<Form.Group>
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control 
						type="text" 
						placeholder="Insert Mobile Number Here" value={mobileNo} maxlength="11"
						onChange={e => setMobileNo(e.target.value)}
						required 
					/>
				</Form.Group>

				{isRegisterBtnActive ? 
					<Button variant="primary" className="btn btn-block" type="submit" id="submitBtn">
						Create New Account
					</Button>
					: 
					<Button variant="danger" className="btn btn-block" type="submit" id="submitBtn" disabled>
						Create New Account
					</Button>
				}
				
			</Form>
		</Container>
	);
}
